# CLR Via C# Notes

Notes on CLR Via C# by Jeffrey Richter


## Table of Contents
### Part 1: CLR Basics
#### 1. [The CLR's Execution Model](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md)  
  1.1 [Compiling Source to Managed Modules](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#11-compiling-source-to-managed-modules)  
  1.2 [Combining Managed Modules into Assemblies](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#12-combining-managed-modules-into-assemblies)  
  1.3 [Loading the Common Language Runtime](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#13-loading-the-common-language-runtime)  
  1.4 [Executing Your Assembly's Code](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#14-executing-your-assemblys-code)  
  1.5 [The Native Code Generator Tool: NGen.exe](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#15-the-native-code-generator-tool-ngenexe)  
  1.6 [The Framework Class Library (FCL)](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#16-the-framework-class-library-fcl)  
  1.7 [The Common Type System (CTS)](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#17-the-common-type-system-cts)  
  1.8 [The Common Language Specification](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#18-the-common-language-specification)  
  1.9 [Interoperability with Unmanaged Code](Part-1-CLR-Basics/Ch1-CLR-Execution-Model.md#19-interoperability-with-unmanaged-code)  

#### 2. [Building, Packaging, Deploying, and Administering Applications and Types](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md)   
  2.1 [.NET Framework Deployment Goals](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md#21-net-framework-deployment-goals)  
  2.2 [Building Types into a Module](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md#22-building-types-into-a-module)  
  2.3 [A Brief Look at Metadata](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md#23-a-brief-look-at-metadata)  
  2.4 [Combining Modules to Form an Assembly](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md#24-combining-modules-to-form-an-assembly)  
  2.5 [Culture](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md#25-culture)  
  2.6 [Simple Application Deployment (Privately Deployed Assemblies)](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md#26-simple-application-deployment-privately-deployed-assemblies)  
  2.7 [Simple Administrative Control (Configuration)](Part-1-CLR-Basics/Ch2-Building-Packaging-Deploying-Administering-Applications-Types.md#27-simple-administrative-control-configuration)

#### 3. [Shared Assemblies and Strongly Named Assemblies](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md)  
  3.1 [Two Kinds of Assemblies, Two Kinds of Deployment](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#31-two-kinds-of-assemblies-two-kinds-of-deployment)  
  3.2 [Giving an Assembly a Strong Name](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#32-giving-an-assembly-a-strong-name)  
  3.3 [The Global Assembly Cache](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#33-the-global-assembly-cache)  
  3.4 [Building an Assembly That References a Strongly Named Assembly](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#34-building-an-assembly-that-references-a-strongly-named-assembly)  
  3.5 [Strongly Named Assemblies Are Tamper-Resistant](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#35-strongly-named-assemblies-are-tamper-resistant)  
  3.6 [Delayed Signing](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#36-delayed-signing)  
  3.7 [Privately Deploying Strongly Named Assemblies](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#37-privately-deploying-strongly-named-assemblies)  
  3.8 [How the Runtime Resolves Type References](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#38-how-the-runtime-resolves-type-references)  
  3.9 [Advanced Administrative Control (Configuration)](Part-1-CLR-Basics/Ch3-Shared-Strongly-Named-Assemblies.md#39-advanced-administrative-control-configuration)  

### Part 2: Designing Types

### Part 3: Essential Types

### Part 4: Core Facilities

### Part 5: Threading

#### 26. [Thread Basics](Part-5-Threading/Ch26-Thread-Basics.md)
  26.1 [Why Does Windows Support Threads?](Part-5-Threading/Ch26-Thread-Basics.md#261-why-does-windows-support-threads)  
  26.2 [Thread Overhead](Part-5-Threading/Ch26-Thread-Basics.md#262-thread-overhead)  
  26.3 [Stop the Madness](Part-5-Threading/Ch26-Thread-Basics.md#263-stop-the-madness)   
  26.4 [CPU Threads](Part-5-Threading/Ch26-Thread-Basics.md#264-cpu-threads)   
  26.5 [CLR Threads and Windows Threads](Part-5-Threading/Ch26-Thread-Basics.md#265-clr-threads-and-windows-threads)   
  26.6 [Using a Dedicated Thread to Perform an Asynchronous Compute-Bound Operation](Part-5-Threading/Ch26-Thread-Basics.md#266-using-a-dedicated-thread-to-perform-an-asynchronous-compute-bound-operation)   
  26.7 [Reasons to Use Threads](Part-5-Threading/Ch26-Thread-Basics.md#267-reasons-to-use-threads)   
  26.8 [Thread Scheduling and Priorities](Part-5-Threading/Ch26-Thread-Basics.md#268-thread-scheduling-and-priorities)   
  26.9 [Foreground Threads versus Background Threads](Part-5-Threading/Ch26-Thread-Basics.md#269-foreground-threads-versus-background-threads)   
  26.10 [What Now?](Part-5-Threading/Ch26-Thread-Basics.md#2610-what-now)   