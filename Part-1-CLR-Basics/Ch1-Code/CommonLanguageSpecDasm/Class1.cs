﻿using System;

namespace CommonLanguageSpecDasm
{
    // This is just a test class so we don't care too much about compiler warnings
    #pragma warning disable CS0660, CS0661
    internal sealed class Test
    {
        // Constructor
        public Test() { }

        // Finalizer
        ~Test() { }

        // Operator overload
        public static Boolean operator ==(Test t1, Test t2) { return true; }
        public static Boolean operator !=(Test t1, Test t2) { return false; }
        public static Test operator +(Test t1, Test t2) { return null; }

        // Property
        public String AProperty
        {
            get { return null; }
            set { }
        }

        // An indexer
        public String this[Int32 x]
        {
            get { return null; }
            set { }
        }

        // An event
        public event EventHandler AnEvent;
        
    }
    #pragma warning restore CS0660, CS0661
}
