# 1.0 The CLR's Execution Model

[Overall TOC](../README.md)


**Contents**

[[_TOC_]]


## 1.1 Compiling Source to Managed Modules

- After deciding on the application you want to build, writing specs, etc you need to decide on a language to use
- C/C++ give you low level control, while VB6 lets you easily build UI forms and interact with COM objects
- The CLR (Common Language Runtime) is...a runtime usable by many different programming languages
    - Core features available to all languages that target it
      - Memory management, assembly loading, security, exception handling, thread sync
    - CLR has no idea what programming language was used by the developer
- Language selection is still important - mainly due to compilers checking syntax and for code correctness
  - Micrososft has several compilers that target the CLR:
    - C++/CLI, C#, Visual Basic, F#, Iron Python, Iron Ruby, IL Assembler
  - Other orgs have made compilers for the CLR for a variety of languages
- Compilation steps can be seen here:  
  - Each language uses its own compiler but we end up with a Managed module at the end
  - The managed module comprises of Intermediate Language (IL) and metadata

    ![](../img/Ch1-1-Compilation.png)

- Native code compilers target a specific CPU architecture (x86/x64/ARM/...)
- CLR compilers instead produce Intermediate Language (IL) code
  - IL code sometimes referred to as "managed code" as the CLR *manages its execution*
  - Compilers also need to emit full metadata into managed modules
    - Set of data tables describing types and their members
    - Also includes information describing what the module references (e.g. imported types and their members)
    - Metadata is included in the .exe or .dll produced by the compiler
- Metadata is useful in a number of ways:
  - Removes the need for C/C++ header files when compiling - all the information about the referenced types/members is contained in the .exe/.dll that implements the types/members, so the compiler can get the information from there
  - IDEs can use this metadata to give you hints while writing code (e.g. type or method parameter hinting)
  - CLR uses metadata to ensure your code only does "type safe" operations
  - Metadata allows for easier network transmission
  - Metadata allows the garbage collector to manage object lifetime
    - For any object, the GC can determine the object type and from the metadata it can work out which fields refer to other objects

- Microsoft compilers/assemblers always produce modules containing IL code. 
  - End users need the CLR installed on their machine to run code compiled with these tools
  - CLR included with the .NET framework
  - Exception to this is the MS C++ compiler, which can produce native and/or IL code

## 1.2 Combining Managed Modules into Assemblies

- CLR doesn't work with modules, it works with assemblies
- Assembly is a logical grouping of one or more modules/resource files
  - Also the smallest unit of reuse/security/versioning
  - Can produce single/multi-file assemblies (compiler option)
- Overall process:  
  - Compilers turn the managed module into an assembly
    - Compilers emit a module containing a manifest  
  
![](../img/Ch1-2-Combining-to-Assemblies.png)

- Assemblies decouple logical/physical parts of a reusable/securable/versionable component
- The partition method is up to you:
  - Could put rarely used types/resources in separate files in the assembly
  - Rarely used types/resources could be downloaded from the web on-demand/at runtime
- Modules also contain info on referenced assemblies
  - Makes assembly "self-describing"
  - CLR can find assembly's dependencies, so you don't need to use the registry/Active Directory Domain Services
  - Makes deploying assemblies easier than deploying unmanaged applications


## 1.3 Loading the Common Language Runtime

- If your assembly just uses type safe managed code, your code will run on both x86 and x64 Windows
  - One file will run on any machine that has a valid version of the .NET framework
- Sometimes you want to only work with a specific version of Windows
  - E.g. when you are interoping with unmanaged code targeted to a specific platform
  - Depending on the platform switch { anycpu | x86 | x64 | arm }, the C# compiler will produce an assembly that contains a PE32/PE32+ header, and the desired CPU architecture
- In the .NET SDK, there are two command line tools for looking at header info: [DumpBin.exe](https://docs.microsoft.com/en-us/cpp/build/reference/dumpbin-reference) and [CorFlags.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/corflags-exe-corflags-conversion-tool)

- **On execution**:
  - Windows looks at the header to determine the address space required by the executable (32/64 bit)
  - Checks the CPU target is valid for the machine (e.g. if CPU target is ARM, x64 Windows won't execute it)
  - Loads the appropriate MSCorEE.dll in to the process' address space
  - Process' primary thread calls an initialization function from MSCorEE.dll
    - This initializes the CLR, loads the EXE assembly and calls the entry point (the `Main()` method)
  - At this point, the managed app is running

## 1.4 Executing Your Assembly's Code

- Intermediate Language (IL) is a CPU-independent machine languagecreated by Microsoft
  - Higher level language than most CPU machine languages
  - Has instructions to:
    - Create/initialize/access/manipulate object types
    - Call virtual methods on objects
    - Directly manipulate array elements
    - Throw/catch exceptions
  - Broadly speaking, can be considered an object-orientated machine language
  - Usually, you will write C#/F#/some other .NET language, and your compiler will compile it to IL
  - You *can* write IL directly, and use [ILasm.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/ilasm-exe-il-assembler) to assemble it. MS also provides [ILDasm.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/ildasm-exe-il-disassembler) which is an IL disassembler
  - The higher-level language will only expose some subset of the CLR facilities, but IL (obviously) exposes everything
  - It's possible to mix languages within an assembly, so you could write one part of the module in C#, one part in IL and a third part in F#

- Executing a method involves first converting the IL to native CPU instructions - this is done by the CLR's Just-In-Time (JIT) compiler:

![](../img/Ch1-3-Initial-Method-Call.png)

- The JIT will produce instructions relevant for the current execution context
  - E.g. if you are running the application on x86 windows or WoW64m the JIT will produce x86 instructions
- At the second call of `Console.WriteLine(string)`, the method has already been JIT-ed so execution jumps to the memory allocated in step 3:

![](../img/Ch1-4-Second-Method-Call.png)

- JIT-ed code is stored in dynamic memory, so the compiled code is discarded when the application terminates
- There is a performance hit on the initial method call, but this is small
- The JIT also optimizes the native code (as a "normal" compiler does)
- Setting the `/optimize-` compiler switch, the IL code contains a lot of NOP and branches that jump to the next line of code - this is to enable debugging/edit-and-continue features of VS
  - When the JIT compiles this to native code, it removes the extraneous instructions
- While the double-compilation process (C# -> IL -> Native) incurs a runtime overhead, a lot of work has been put in to minimizing the impact
- In some cases, managed applications can outperform unmanaged applications:
  - When the JIT is running, it knows more about the execution environment than an unmanaged compiler can know - there's more info available at run time than at compile time
  - The JIT can produce instructions specific to the CPU executing the code - native compilers usually just optimise for the "lowest common denominator" CPU
  - A JIT can determine if a test is always false, e.g. the following test could be optimized out:
  ```C#
  if (numberOfCPUs > 1) {
      ...
  }
  ```
  - The CLR could profile code execution and recompile better IL *during application execution*
    - This might include reducing incorrect branch predictions
    - This isn't implemented currently, but could be in the future
- If you find the JIT has too much of a performance hit, you can use [NGen.exe](#15-the-native-code-generator-tool-ngenexe) to generate a native image
  - This compiles all of an assembly and saves to disk
  - At runtime, the CLR checks for a native image and uses that if it is available
- You can also use the `System.Runtime.ProfileOptimization` class
  - Makes the CLR record the methods that are JIT-ed during execution (to a file)
  - On future application start, the JIT can pre-emptively compile these methods using background threads

### Intermediate Language and Verification

- IL is stack-based: operations push operands on to stack and pop results off stack
- IL has no register manipulation so new languages/compilers can be relatively easily made to produce code targeting the CLR
- IL instructions are also typeless - when the instruction is executed, the runtime determines the type of operands on the stack and performs the correct instruction
- During the compilation process, the CLR examines the IL code and ensures that everything it does is safe - this is called "verification" and includes:
  - Checking all methods are called with the correct number of parameters
  - Method parameters are the correct type
  - Method return values are used properly
- Verification gives you a runtime guarantee that code does not improperly use memory, so you can run multiple managed applications within the same virtual address space
- The CLR allows for the execution of multiple managed applications in a single address space
  - Each managed process executes in an AppDomain
  - By default, managed executables run in their own AppDomain (and so their own process/address space)
  - Some processes host the CLR (e.g. IIS or SQL Server), and can decide to run AppDomains in a single OS process/address space

### Unsafe Code

- By default, the MS C# compiler produces verifiably safe code
- With a command line flag (`/unsafe`), you can also write unsafe code
  - This basically just allows you to dereference raw pointers, and manipulate bytes at the dereferenced addresses
- This is usually used when interoperating with unmanaged code, or eking out the last bit of performance in a time-critical algorithm
- With great power comes great responsibility: unsafe code can corrupt data structures and introduce security vulnerabilities
  - Because of this, all methods that contain unsafe code require the `unsafe` keyword
- When the JIT goes to compile an unsafe method, it checks to see if the assembly containing the method has the `System.Security.Permissions.SecurityPermissionFlag.SkipVerification` flag set
  - If this flag is set, the JIT will compile the unsafe method and execute it - the CLR trusts/hopes that the unsafe components of the method don't corrupt memory
  - If the flag is not set, the JIT throws either of `System.InvalidProgramException` or `System.Security.VerificationException` exceptions, preventing the method from executing
  - This will probably terminate the application, but it prevents memory corruption
  - By default, assemblies from the local machine or network shares can execute unsafe code, but assemblies executed via the internet are not allowed to execute unsafe code
- Microsoft supplies a utility called [PEVerify.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/peverify-exe-peverify-tool), which checks an assembly (and all of the assemblies it depends on) for unsafe code

### IL and Protecting Intellectual Property

- Because IL is higher-level than "normal" assembly languages, reverse-engineering IL code is simpler than native code
- ILDasm can be used to disassemble IL code for reverse engineering
- If this is a concern, it's possible to use an obfuscator to scramble names in the metadata of the assembly
  - Obfuscators only provide a little protection because the IL must be available for the CLR to JIT
- If an obfuscator does not provide protection, you can write the IP-sensitive components in an unmanaged module and interop with it

## 1.5 The Native Code Generator Tool: NGen.exe

- The [NGen.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/ngen-exe-native-image-generator) tool can be use to compile IL to native code during the installation process
- Since the code is compiled at installation time, the JIT does not need to do anything at runtime. This can:
  - Improve startup times - the code is already compiled
  - Reduce an application's working set
    - If an assembly will be loaded in to many processes simultaneously, NGen-ing that assembly will produce a file (of native instructions).
    - This file can be memory mapped in to multiple process address spaces simultaneously (as it's just a code file)
- When NGen is invoked, all relevant assemblies are compiled from IL to native code
- A new assembly containing just the native code is placed in a subfolder of `%SystemRoot%\Assembly\NativeImages_v4.0.<version>_<arch>\`
  - The folder will denote the .NET framework version and the target architecture
- When the CLR loads an assembly, it will check to see if there is a corresponding NGen'd image

### Issues with NGen

- You need to ship both the IL files and the NGen'd images, so you don't get IP protection
  - At runtime, the CLR needs access to the assembly metadata
  - If the CLR has issues with the NGen'd image, the CLR gracefully reverts to JIT compilation
- Ngen'd files are specific to a number of execution environment characteristics:
  - CLR version - changes with patches/service packs
  - CPU type
  - Windows OS - changes with service packs
  - Assembly identity module version ID - changes with recompilation
  - Referenced assembly version IDs - changes when you recompile a referenced assembly
  - Security - changes when you revoke permissions (e.g. declarative inheritance, declarative link-time, SkipVerification or UnmanagedCode permissions)
- Worse execution performance: NGen.exe doesn't have as much information about the execution environment as the JIT does
- NGen'd images can perform about 5% slower than the JIT'ed counterpart!

- When making decisions on "to NGen or not to NGen", you can use MS's [Managed Profile Guided Optimization tool (MGPO.exe)](https://docs.microsoft.com/en-us/dotnet/framework/tools/mpgo-exe-managed-profile-guided-optimization-tool)
  - Analyses the execution of your program to see what it needs at startup
  - Results can be fed to NGen to better optimize the native image

## 1.6 The Framework Class Library (FCL)

- The FCL is a set of assemblies that contain several thousand type definitions
- These include libraries for:
  - Web services - ASP.NET, Windows Communication Foundation (WCF)
  - Web forms/MVC HTML Web Apps - ASP.NET
  - Windows GUI apps - Windows Store, Windows Presentation Foundation (WPF), Windows Forms
  - Console Applications
  - Windows services
  - Database stored procedures
  - Component Libraries
- These types are split up in to namespaces
  - The `System` namespace includes the `Object` base type (which all other types are derived from), as well as types for integers, characters, strings, exceptions and handling, console I/O
  - Also includes utility types that perform safe type casting, data formatting, random number generation
- Many types allow you to customise type behaviour by creating derived types

Namespace | Contents
---|---
`System` | Basic types used by every application
`System.Data` | Types for communicating with a database, processing data
`System.IO` | Types for doing stream I/O, file/directory interactions
`System.Net` | Low level network communications, working with common internet protocols
`System.Runtime.InteropServices`| Types for managed code to access unmanaged OS facilities (e.g. COM components, functions in Win32/custom DLLs)
`System.Security` | Types for protecting data/resources
`System.Text` | Types for working with text (including ASCII and Unicode)
`System.Threading` | Types for async operations and synchronizing access to resources
`System.Xml` | Types for processing XML schemas and data

## 1.7 The Common Type System (CTS)

- Unsurprisingly, the CLR is all about types
  - Types allow code written in one language to interact with code written in another
- Because types are the root of the CLR, there is a formal (MS-created) spec for defining types and how they behave - the Common Type System
  - This and other aspects of the .NET framework have been submitted to Ecma (ICT systems standardization body) for standardization. This includes file formats, metadata structure, IL and P/Invoke
  - This standard is the "Common Language Infrastructure" (CLI) and forms the ECMA-335 Spec
  - Other aspects of the .NET spec have also been submitted to Ecma including:
    - Portions of the Framework Class Library
    - The C# programming language (ECMA-334)
    - The C++/CLI programming language

- The Common Type System (CTS) states that a type can contain sero or more members. Members can be any of the following:

Type of Member | Description
---|---
Field | Data variable, part of the object's state. Identified by name and type.
Method | Function that performs an operation on the object, often modifying state. <br>Have a name, signature and modifiers. <br>Signature specifies number of and type of parameters, return type of method (if any).
Property | Appears to be a field to the caller, but to the implementer it appears to be one or two methods. <br>Allows the implementer to validate input, or only calculate a value when necessary. <br>Allows for simplified syntax for users of the type, and allows for read-only fields.
Event | Allows a notification mechanism between an object and other "interested" objects. <br> E.g. a button could offer an event that notifies other objects when the button is pressed.

- The CTS also specifies rules for type visibility and type member access
  - **Public** (in C#, `public`) exports the type, so that type is visible/usable by any assembly
  - **Assembly** (in C#, `internal`) keyword makes the type visible/usable only by that assembly
  - **Private** (in C#, `private`) keyword makes the member accessible by other members in the same class
  - **Family** (in C#, `protected`) keyword is accessible by derived types of the containing class
  - **Family or assembly** (in C#, `protected internal`) keywords mean the member is accessible by derived types in any assembly, or by any types in the same assembly
- The CTS defines rules around type inheritance, virtual methods, object lifetime and so on
  - You don't need to learn the CTS rules, as these rules are implemented by your CLR language of choice, and the compiler will ensure that the generated IL complies with the rules
- The CTS spec ensures that type behaviour is independent of language implementation

- One rule in the CTS is that all types must inherit (eventually) from `System.Object`. This root type allows you to:
  - Compare two instances for equality
  - Get a hash code for the instance
  - Query the type of the instance
  - Perform a shallow copy of the instance
  - Obtain a string of the instance object

## 1.8 The Common Language Specification

- COM allows objects from different languages to communicate
- CLR integrates languages and allows objects created in one language to be "first class citizens" in another language
  - Possible because of the Framework Class Library, metadata and the CLR
- Problem: languages don't always have an overlapping syntax
- If you want to create types that work across languages, you need to design for the lowest common denominator - features/syntax that are available in all languages
- MS have defined a Common Language Specification (CLS) that defines the features required by a compiler so that the compiler will be compatible with other CLS-compliant compilers

![](../img/Ch1-4-CLS.png)

- If you don't care about inter-language use of your types, you can build your types with all of the features of your chosen language
- CLS rules don't apply to code that is only accessible within the defining assembly (e.g. `internal`)
- If you do want language interop, make sure that your `public` and `protected` members are CTS compliant
- An example of compiler warnings generated by non-CTS compliant code can be seen below:
```cs
using System;

// Tell compiler we want to check for CTS compliance  
[assembly: CLSCompliant(true)]

namespace SomeLibrary {  
  // Warnings appear because the class is public  
  public sealed class SomeLibraryType {  
    
    // Warning: Return type of 'SomeLibrary.SomeLibraryType.Abc()'
    // is not CLS-compliant
    public Uint32 Abc() { return 0; }  

    // Warning: Identifier 'SomeLibrary.SomeLibraryType.abc()'
    // differing only in case is not CLS-compliant
    public void abc() { }

    // No warning: the method is private
    private UInt32 ABC() { return 0; }
  }
}
```
- Put simply: in the CLR every member of a type is either a field (data) or a method (behaviour)
- Every language should be able to access fields and call methods
  - Certain fields and methods are used in special/common ways
  - CLR compilers need to translate the special constructs in your code in to fields and methods to give any other language for the CLR access to those types

- Consider the following example (and bad) code:

```cs
using System;

internal sealed class Test {
  // Constructor
  public Test() {}

  // Finalizer
  ~Test() {}

  // Operator overload
  public static Boolean operator == (Test t1, Test t2) { return true;  }
  public static Boolean operator != (Test t1, Test t2) { return false; }
  public static Test    operator +  (Test t1, Test t2) { return null;  }

  // Property
  public String AProperty {
    get { return null; }
    set { }
  }

  // An indexer
  public String this[Int32 x] {
    get { return null; }
    set { }
  }
  
  // An event
  public event EventHandler AnEvent;

}

```

- When the compiler compiles this code, there will be a type that has a number of fields and methods
  - You could disassemble this code to view the managed module

Type Member | Member Type | Equivalent language construct
---|---|---
AnEvent | Field | Event
.ctor | Method | Constructor
Finalize | Method | Finalizer
add_AnEvent | Method | Event add accessor method
get_AProperty | Method | Property get accessor
get_Item | Method | Indexer get accessor
op_Addition | Method | + operator
op_Equality | Method | == operator
op_Inequality | Method | != operator
remove_AnEvent | Method | Event remove accessor 
set_AProperty | Method | Property set accessor
set_Item | Method | Indexer set accessor

- There is additional type metadata in `.class`, `.custom`, `AnEvent`, `AProperty` and `Item` produced in ILDasm

- See ILDasm output [here](Ch1-Code/CLSDasm.html)

## 1.9 Interoperability with Unmanaged Code

- The CLR has interoperability with unmanaged code
- Functions in three scenarios:

- **Managed code calling unmanaged functions in a DLL**
  - Managed code can call functions in DLLs using something called `P/Invoke` (Platform Invoke)
  - Many types in the FCL internally use P/Invoke to call functions from Windows DLLs (e.g. Kernel32.dll, User32.dll), 
- **Managed code using an existing COM component (server)**
  - Using the type library from the COM component, you can create a managed assembly that describes the COM component
  - MS ships the [TlbImp.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/tlbimp-exe-type-library-importer) tool with the .NET SDK to do this
    - TlbImp.exe: converts type definitions found in a COM type library into equivalent definitions in a CLR assembly
- **Unmanaged code using a managed type (server)**
  - You can expose managed code via a COM interface
  - MS ships [TlbExp.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/tlbexp-exe-type-library-exporter) and [RegAsm.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/regasm-exe-assembly-registration-tool) in the .NET SDK to do this
    - TlbExp.exe: generates a type library describing the types defined in a CLR assembly
    - RegAsm.exe: reads metadata within an assembly, adds required registry entries so that COM clients can interact with .NET classes



