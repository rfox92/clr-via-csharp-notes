# 2.0 Building, Packaging, Deploying, and Administering Applications and Types

[Overall TOC](../README.md)

**Contents**

[[_TOC_]]

## 2.1 .NET Framework Deployment Goals

- Windows has a reputation for being unstable and complicated
  - Applications use DLLs from MS and other vendors
  - Application updates ***should*** be backwards-compatible with anything that uses the DLL's functionality
  - This is not always the case: hence, "DLL hell"
  - Typical application install process involves:
    - Copying files to various locations
    - Creating/editing registry keys
    - Adding shortcuts to the start menu
  - This means the application is not isolated as a single entity!
    - Moving the install directory is non-trivial
    - You can't move the application from one machine to another
    - You can't be sure that uninstalling the application will actually purge all of the application from your machine
  - Some web applications might have ActiveX controls, which download and run code on the user's machine
    - This is likely to be opaque to the user!
    - The code can perform a pretty wide variety of operations including deleting files and sending emails  


- The .NET Framework aims to resolve all of this:
  - Application state is not so scattered over the user's hard disk
    - Unlike COM, .NET types no longer require registry settings
  - .NET includes a security model called "code access security"
    - Windows security is based on user identity, .NET security allows hosts to set permissions controlling what loaded components can do
    - E.g. SQL Server can grant just a few permissions, whereas a self-hosting application can give itself full access
  - The .NET Framework gives users much more control over what's installed and what runs

## 2.2 Building Types into a Module

- See [Program.md](Ch2-Code/Program.md) and [build.md](Ch2-Code/build.md) for reference
- When `csc` processes `Program.cs`, it sees that the code references `System.Console.WriteLine()` (which is not in the file)
- Luckily, we pass `/r:MSCorLib.dll` to `csc` so it knows to look in that DLL for referenced types
  - `MSCorLib.dll` includes all core types: `Byte`,`Char`,`String`,`Int32`, etc
  - This file is referenced so often that `csc` by default already references it
- Using the `/r[eference]` compiler switch, you can either specify a full path or a file name
  - If you specify a filename, the compiler will look in the following locations (in the following order) for matching references:
    1. Current working directory
    2. Directory containing `csc`
    3. Any directories specified with the /LIB compiler switch
    4. Any directories specified with the `$env:LIB` environment variable

### Response files
- Response files are text files containing command-line switches
- When you execute `csc`, the compiler will add any switches in the response files to the command line arguments
- You specify a response file by adding `@<responseFileName>` to the command line switches of `csc`
- You can use multiple response files
- There's a default `CSC.rsp` file containing global compiler settings - it mainly refers to a heap of System DLLs
- Referencing these DLLs can slow compilation times, but it doesn't slow runtime/have any effect on runtime performance or generated IL
- You can specify `/noconfig` and `csc` will ignore both local and global RSP files

## 2.3 A Brief Look at Metadata

- What's in a Portable Executable (PE) file?
  - PE32(+) Header
    - Standard image information expected by Windows
  - CLR header
    - Includes version info for the CLR that the module was built for
    - Additional flags
    - MethodDef token indicating the module entry point
    - Optional strong name signature
    - Size and offsets of metadata tables within the module
  - Metadata
    - Block of binary data consisting of several tables
      - Definition tables
      - Reference tables
      - Manifest tables
  - IL
    - You should know what this is by now

Common Metadata tables:

Table Name | Description
---|---
**ModuleDef** | Module Identification.<br>Module file name and extension, module version ID (GUID created by compiler).
**TypeDef** | One entry for each type definied in module.<br>Includes typename, base type and flags (`public`, `private`, etc.) <br>Also includes indexes to methods for the type in the MethodDef table, fields it owns in the FieldDef table, properties it owns in the PropertyDef table and events it owns in the EventDef table.
**MethodDef** | Entries for all methods in the module <br>Includes method name, signature, flags (`private`, `public`,`virtual`, `static`, etc.)<br> Also includes offset to the method's IL in the module.
**FieldDef** | Entries for all fields in the module, includes flags/type/name
**ParamDef** | Entries for all parameters in the module, including flags/type/name
**PropertyDef** | One entry for each property in the module. Flags, type, name.
**EventDef** | Entry for each event in the module. Flags and name

- During the compilation process, all definitions add entries to the above tables.
- Compiler also detects referenced types/fields/methods/properties/events and adds them to the appropriate (reference) table (e.g. AssemblyRef, ModuleRef, TypeRef, MemberRef)
- ILDasm can view metadata within managed PE file

## 2.4 Combining Modules to Form an Assembly

- `Program.exe` described in [2.2](#22-building-types-into-a-module) is more than a PE file with metadata - it's a whole assembly!
- An assembly is a collection of one or more files containing type definitions and resource files
  - One of those files is chosen to hold a manifest - another set of metadata tables that contains the filenames in the assembly
  - Also describe version, culture, publisher, publicly exported types
- CLR operates on assemblies - it loads the manifest metadata tables first, then uses that to load the rest of the assembly

Assembly hot facts:
  - Assemblies define *reusable* types
  - Assemblies are marked with a *version* number
  - Assemblies can have associated *security* info
  - These attributes are only contained in the assembly file that contains the manifest metadata for the assembly

So, why use Assemblies anyway?
- You can partition types among several files, so that files only need to be downloaded as required (if you don't include the entire assembly at install time)
- You can add resource/data files to the assembly: e.g. you could include steam tables - not in the DLL but in a separate file. This file would be considered part of the assembly after it has been linked by [al.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/al-exe-assembly-linker)
- You can create assemblies consisting of types implemented in different programming languages

## 2.5 Culture

- Assemblies can have a culture as a part of their identity
- Different assemblies can be created for different cultures
  - E.g. different assemblies for German and Swiss German
- Cultures are identified by a string containing a primary and secondary tag
  - Full details in RFC 1766
- Generally, assemblies don't have a culture assigned because code doesn't usually have culture-specific assumptions in it
- If you create an assembly that is culture specific, it's best practice to also produce an assembly with "default" resources (i.e. culture-neutral)

## 2.6 Simple Application Deployment (Privately Deployed Assemblies)

- Couple of different ways to package an application:
  - Windows Store requires you to package everything together in to an .appx file
    - Windows store applications are managed by reference counting: they are only removed from the machine when no users on that machine have it "installed"
  - Desktop (non-Windows store) applications don't have a specific means of packaging
    - Simple packaging can involve just copying files to the user's machine
    - Uninstallation just means deleting the files!
  - Can also use MSI packages for installation
    - Using .msi files allows you to install assemblies on demand - the first time the CLR attempts to load the assembly
    - Using .msi files also allows you to install prerequisites (e.g. the .NET framework or MS SQL Server Express)

- Assemblies deployed to the application directory are "privately deployed assemblies"
  - The assembly is not shared with applications outside of the directory

## 2.7 Simple Administrative Control (Configuration)

- Trust admins/users with some aspects of application execution!
  - Admins might decide to move assembly files on the user's hard disk or to override info in the assembly manifest
  - Sometimes there's weird versioning things going on (see [Chapter 3](Ch3-Shared-Strongly-Named-Assemblies.md))
- To allow admin control over an application, a config file can be placed in the application directory
  - Publisher can make a default version of this file
  - CLR uses the content of the file to alter policies for locating/loading assembly files
  - Config files XML based, associated with one application or with the whole machine
  - Config files >>> regisry as settings are self-contained, easy to back up and transfer

Consider the following directory layout:
```C
AppDir/ // Application assembly files here
  ├── Program.exe
  ├── Program.exe.config
  └── AuxFiles/ // Subfolder, contains some files for MultiFileLibrary
      ├── MultiFileLibrary.dll
      ├── FUT.netmodule
      └── RUT.netmodule
```
- Because MultiFileLibrary is not in the base directory, the CLR cannot find the files
  - Will throw `System.IO.FileNotFoundException`
- To fix this, we can create an XML config file and put it in the base directory
  - Name of file must be the name of the application's main assembly file with a .config extension
- In our case, the config file will look like this:

```XML
<configuration>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <probing privatePath="AuxFiles">
    </assemblyBinding>
  </runtime>
</configuration>
```

- The CLR will try to locate an assembly by first looking in the application directory then in the AuxFiles subdir
  - Multiple (semicolon separated) paths are valid
  - All paths are relative to the base directory
  - All paths must be within the base dir
