```powershell

$csc = "${Env:ProgramFiles(x86)}\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\Roslyn\csc.exe"

# target is an exe
# we reference MSCorLib.dll
& $csc /out:Program.exe /t:exe /r:MSCorLib.dll Program.cs

# Write the contents of Program.cs to markdown
$prog = Get-Content .\Program.cs -Raw
$prog = '```cs' + "

" + $prog + "

" + '```'
Out-File -FilePath Program.md -InputObject $prog

# Write the contents of this file to markdown
$build = Get-Content $PSCommandPath -Raw
$build = '```ps

'  + $build + '

```'
Out-File -FilePath build.md -InputObject $build

```
