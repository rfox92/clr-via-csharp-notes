```cs

// Define a type, called "Program"
public sealed class Program {
    // Only method
    public static void Main() {
        // Reference to System.Console type
        // Code for System.Console is in MSCorLib.dll
        System.Console.WriteLine("Hello world!"); 
        System.Console.WriteLine("This is a test!");
    }
}

```
