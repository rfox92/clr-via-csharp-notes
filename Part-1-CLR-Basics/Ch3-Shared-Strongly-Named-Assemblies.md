# 3.0 Shared Assemblies and Strongly Named Assemblies

[Overall TOC](../README.md)

**Contents**

[[_TOC_]]

- Previously, we've looked at "privately deployed assemblies"
  - Assemblies deployed to the application's base directory or a subdirectory
  - Privately deployed assemblies give users/admins a highlevel of control over assembly naming/versioning/behaviour
  - These assemblies are only available to a small subset of applications - applications in the assembly's parent path
- Here we'll talk about assemblies accessible to all applications on the machine - ***globally deployed assemblies***
  - Common example of a globally deployed assembly: the [framework class library](Ch1-CLR-Execution-Model.md#16-the-framework-class-library-fcl)

- The .NET Framework has a lot of infrastructure to deal with versioning issues
  - This is complicated because versioning is complicated
  - There's a lot of policies/rules built in to the CLR
  - There's also s number of tools/utilities for devs

## 3.1 Two Kinds of Assemblies, Two Kinds of Deployment

- There's two types of assemblies: "weakly named assemblies" and "strongly named assemblies"
- Both are structurally identical
  - Use the same portable executable (PE) format, PE32(+) header, CLR header, manifest, metadata and IL
- Key difference is that strongly named assemblies are signed with the publisher's public/private key pair
  - Uniquely identifies the software
  - Allows the assembly to be deployed anywhere on the user's machine
- Uniquely identified assemblies let the CLR enforce "known-safe" policies when applications try to use that assembly

- Weakly named assemblies can *only* be deployed privately
- Strongly named assemblies can be privately *or* globally deployed

## 3.2 Giving an Assembly a Strong Name

- If multiple applications want to use an assembly, the assembly must be placed in a well known (by the CLR) directory
- If more than one organisation produce an assembly with the same file name, and that file is just dumped in to a common directory, then we end up where we started: DLL hell
  - Differentiating assemblies by name isn't good enough
- This is where the term "strongly named" assembly comes from
  - The strong name consists of four aspects:
    - File name (with no extension)
    - Version Number
    - Culture Identity
    - Public key - this identifies the publisher
      - We often use a small hash created from the full public key called the "public key token"
- You can use the `/keyfile:<file>` csc compiler switch, which will sign the assembly with the private key and embed the public key in the manifest
- During the signing process, all files are hashed and signed before their entries are added to the CLR header


## 3.3 The Global Assembly Cache

- If an assembly is going to be used by multiple applications, it needs to be in a well-known (by the CLR) location
  - This location is known as the Global Assembly Cache (GAC), typically in `%SystemRoot%\Microsoft .NET\Assembly`
  - The GAC is structured by the CLR - you should never manually copy files to the GAC, instead use tools to register assemblies
  - Common tools are [GACutil.exe](https://docs.microsoft.com/en-us/dotnet/framework/tools/gacutil-exe-gac-tool) for registering/unregistering strongly-named assemblies in dev
  - Weakly-named assemblies cannot be registered in the GAC
  - Windows Installer (MSI) is the only GAC tool guaranteed to be on end-user machines
  - GAC registration breaks the goal of simple application installation/backup/restore/move/uninstall
- What's the point of the GAC then?
  - If two developers produce a strongly-named OurLibrary.dll assembly, the two versions obviously can't go in to the same directory
  - GACUtil or MSI will put the DLLs in separate directories


## 3.4 Building an Assembly That References a Strongly Named Assembly

- All assemblies will need to reference another strongly-named assembly, if only to reference `MSCorLib.dll` to get `System.Object`
- You can use the `/reference` `csc` compiler switch to specify assembly files you want to reference
  - If you only specify a filename, `csc.exe` will look in the following places (in order)
    - Current working directory
    - Directory that contains `csc.exe` - this includes the CLR DLLs
    - Directories specified using `/lib`
    - Directories specified using `$env:LIB`
- When you install the .NET Framework, two copies of assembly files are installed:
  - One in the compiler/CLR directory - so that you can build your assembly
  - One in the GAC - so that you can load the strongly-named assembly at runtime
- `csc.exe` doesn't look in the GAC for referenced assemblies because the GAC structure is undocumented
  - The alternative would be to specify, for example, `"System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a”`
  - This was deemed worse than having two copies installed on a hard drive
  - `rfox:` This seems like a bad excuse: Microsoft make both the GAC management tools and `csc.exe`, so they could surely get the two tools to play nice
- Assemblies in the CLR/compiler directory are just metadata assemblies - we don't need IL at build time so it doesn't contain the x86/x64/arm versions of an assembly
  - The GAC contains metadata and IL code as these are both needed at runtime


## 3.5 Strongly Named Assemblies Are Tamper-Resistant

- Signing an assembly allows the CLR to verify that the assembly has not been modified since the publisher's compiler spat out the assembly
  - This is done at both installation time and at runtime
  - The CLR will throw a `System.IO.FileLoadException` at runtime if a file has been tampered with (i.e. the hashes do not match)

## 3.6 Delayed Signing

## 3.7 Privately Deploying Strongly Named Assemblies

## 3.8 How the Runtime Resolves Type References

## 3.9 Advanced Administrative Control (Configuration) 