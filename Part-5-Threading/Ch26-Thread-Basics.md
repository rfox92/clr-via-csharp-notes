# 26.0 Thread Basics

[Overall TOC](../README.md)

**Contents**

[[_TOC_]]

## 26.1 Why Does Windows Support Threads?

- Old OSes just worked on batch execution, so a misbehaving application could just lock up an entire machine
  - Only solution was a reboot
  - This included 16-bit Windows
- WinNT needed to be more robust, reliable, scalable, secure
  - Also shouldn't lock up the machine when an application does a bad thing
  - WinNT Kernel designed to run each app instance in a process
    - Process is a unit of resource allocation
    - Given a virtual address space, isolating processes from one another
    - Also isolates the OS kernel from applications
  - WinNT kernel also uses threads - units of execution
    - Threads *virtualise* the CPU
    - Each process starts with one thread (granted by the OS), but processes can acquire more threads
    - If one app freezes, it only locks up that one application - other processes keep running

## 26.2 Thread Overhead

- Threads allow you to have a responsive UI/OS even while part of the application is executing a long-running task
- They also allow users to use one application to kill another
- On the other hand, threads have space (additional memory consumption) and time (execution performance) overheads associated with them
- All Windows threads have the following overheads:
  - **Thread kernel object**: Managed by the OS, contains thread metadata (including thread context)
    - Thread context is the state of the CPU registers
  - **Thread environment block (TEB)**: Block of memory easily accessed by the application
    - Includes execution handling chain, thread local storage data and a few graphics data structures
  - **User mode stack**: local variables, arguments passed to methods, as well which thread should execute when current method returns
    - Windows allocates 1MB per thread of stack space by default
  - **Kernel-mode stack**: used when application code passes arguments to a kernel mode OS function
    - Arguments are copied from user mode stack to kernel mode stack on system call so that user mode can't edit params after syscall
    - System calls will probably result in additional internal methods being called, which requires a stack but we don't want this to be controllable by the application
    - Kernel mode stack is 12KB on Win32 and 24KB on Win64
  - **DLL thread-attach and thread-detach notifications**
    - Whenever a thread is created in a process, all *unmanaged* DLLs have their `DllMain` method called, passing a `DLL_THREAD_ATTACH` flag
    - Whevever a thread is killed in a process, all *unmanaged* DLLs have their `DllMain` method called, passing a `DLL_THREAD_DETACH` flag
    - Some libraries use these notifications to perform special init/cleanup for every thread created/destroyed in the process
    - E.g. the `ucrt.dll` allocates some additional thread-local storage if the thread uses functions in the C runtime library
    - This is the big thing that affects performance with thread creation/desrtuction - if you have 450 DLLs loaded (not uncommon for Visual Studio), then 450 methods need to be called on thread creation, before the thread can do its thing
    - Managed DLLs do not have this overhead. Unmanaged DLLs can also opt out by calling `DisableThreadLibraryCalls` function - though this is not always done

- On top of this, at any instant in time a CPU can only be executing one instruction from one thread
  - This means that to (appear to) execute multiple things simultaneously, we need some way of switching between threads - the ***context switch***
  - Windows (like most modern OSes) has a thread scheduler - this allocates threads to be executed in time slices. At the end of a time slice, the scheduler switches the CPU context. This is done by:
    1.  Save CPU register values to the current thread's context structure inside the thread kernel object
    2.  Select one thread to be scheduled next - if this new thread is from a different process then the OS needs to switch the virtual address space seen by the CPU before any execution occurs
    3.  Load the new thread's context structure in to the CPU registers
  - Windows switches threads approximately every 30ms
  - Context switches are all overhead that is required to provide end users with a robust/responsive OS
  - Additional overhead exists when the new thread is loaded - the old program's code/data will likely be sitting in CPU caches but if we're changing processes, changing the virtual address space means that we lose the benefits of that cache
  - Threads can pre-empt the thread scheduler by yielding to the OS (saying "hey I'm good for the rest of this time slice"), this triggers an immediate context switch

- Within the CLR, the Garbage Collector needs to:
  - Suspend all threads
  - Walk their stacks to mark objects in the heap (potentially multiple times if compaction is occurring)
  - Resume all threads

- All of this means that creating many threads is a bad idea, and may slow your application down
- Luckily the CLR has some mechanisms to help you create fewer threads, minimising the overhead


## 26.3 CLR Threads and Windows Threads

- In the early days of .NET framework, the CLR team considered offering the CLR offer logical threads that did not necessarily map to OS threads
- This attempt was unsuccessful, but vestiges remain:
  - `System.Environment.CurrentManagedThreadId` returns the CLR's ID for a thread
  - `System.Diagnostics.ProcessThread.Id` returns Windows' ID for a thread
  - `System.Thread.BeginThreadAffinity()` and `System.Thread.EndThreadAffinity()` exist to deal with the idea that a CLR thread may not map exactly to a Windows thread

## 26.4 Using a Dedicated Thread to Perform an Asynchronous Compute-Bound Operation

- This technique is not generally recommended, instead it's recommended to use a thread pool as in Chapter 27
- Some situations mean that you might want to explicitly create a thread for one specific operation
- Tyically you want to create dedicated threads if you execute code that requires the thread to be in an abnormal state (e.g. one that a thread pool is not in)
- If any of the following is true then you probably want to explicitly create your own thread:
  - The thread needs to be run at non-normal thread priority 
    - All thread pool threads run at normal thread priority. This can be changed but the change does not persist across thread pool operations
  - The thread needs to behave as a foreground thread, preventing the application from terminating until the thread has completed its work
    - Thread pool threads may not complete their task if the CLR decides to terminate the process - they are always background threads
  - The task is extremely long-running (or always running)
    - If this is managed by the thread pool, it is doing unnecessary work in deciding when to create/remove additional threads - you know that this thread will always be required
  - The thread may be prematurely aborted by calling `Thread.Abort()`

- To do this, you need to construct a `System.Threading.Thread`, passing the name of a method into its constructor

```cs
public sealed class Thread : CriticalFinalizerObject, ... {
  public Thread(ParameterizedThreadStart start);
  // Some more constructors that you'll never call
}
```

- The `start` parameter identifies the method executed by the thread, and its signature must match the signature of the following:

```cs
delegate void ParameterizedThreadStart(Object obj)
```

- This is a fast operation as it does not construct an OS thread until you call `Thread.Start`

```cs
using System;
using System.Threading;

public class Program {
  public static void Main() {
    Console.WriteLine("Main thread: starting a dedicated thread");
    Thread dedicatedThread = new Thread(ComputeBoundOp);
    dedicatedThread.Start(5);

    Console.WriteLine("Main thread: doing some other work");
    Thread.Sleep(10000);
    dedicatedThread.Join(); // wait for our ComputeBoundOp to complete
    Console.WriteLine("Hit <Enter> to end the program");
    Console.ReadLine();
  }

  private static void ComputeBoundOp(Object state) {

    Console.WriteLine("In ComputeBoundOp: state={0}", state);
    Thread.Sleep(1000);
    Console.WriteLine("ComputeBoundOp completed");
  }

}

```

- Note that `Main()` calls `Thread.Join()`, halting the execution of `Main()` until the thread returns

## 26.5 Reasons to Use Threads

- **Responsiveness (particularly for client side GUIs)**
  - Each process gets its own thread so that one process locking up does not affect the system
  - Similarly, a process can spawn threads so that the GUI remains responsive
  - Spawning additional threads here may hurt performance (due to excessive context switching), but the user gets a more responsive application
- **Performance**
  - OSes can schedule 1 thread per CPU, and different CPUs can execute the threads concurrently, your application can get better performance by having multiple operations executing simultaneously

## 26.6 Thread Scheduling and Priorities

- Pre-emptive Operating Systems need to use some sort of algorithm to handle thread scheduling. Here, we look at the Windows implementation
- Earlier we've mentioned that the thread's kernel object contains a context structure
  - This reflects the state of the CPU registers when the thread was last executing
- At the end of a time slice, Windows looks at all of the thread kernel objects (considering all of the threads that are not in the `wait` state)
  - Windows chooses one thread that is schedulable, and keeps track of the number of times a thread has been scheduled
- The selected thread then continues to execute for the duration of the time slice, before the scheduler performs another context switch

- Pre-emptive multithreaded operating systems mean that a thread can be stopped at any time and another thread can be scheduled 
  - Developers have some control over this, but not much

- All threads are assigned a priority of between 0 and 31
  - Thread scheduler round-robins all p31 threads until all have finished executing
  - Then it round-robins all p30 threads and so on
  - p0-p30 threads can therefore be "starved" if no p31 threads ever finish executing and so they keep sharing the CPU amongst themselves
  - In practice this is rare as multicore CPUs somewhat prevent this issue
  - On boot, there is also a p0 thread created called the "zero page thread" which zeroes unused RAM pages - as it's p0 then it is only scheduled when the CPU would otherwise be idle

- Controlling the priority level is hard to rationalize - should we be p10 or p11?
  - Windows exposes an abstraction over the numeric priority
    - Six *process* priority classes: `Idle`, `Below Normal`, `Normal`, `Above Normal`, `High` and `Realtime`
      - Default of `Normal`
      - `Realtime` can interfere with disk IO, network traffic, processing user input (from kb/m), but threads can't be put in `Realtime` unless the user has the "Increase Scheduling Priority" privilege - granted to Administrators and Power Users
    - Seven *thread*-relative priority classes: `Idle`, `Lowest`, `Below Normal`, `Normal`, `Above Normal`, `Highest` and `Time Critical`
      - Again, default of `Normal`



Priority      | Idle  | Below Normal | **Normal** | Above Normal  | High  | Real time
--------------|-------|--------------|------------|---------------|-------|---
Time Critical | 15    | 15           | 15         | 15            | 15    | 31
Highest       | 6     | 8            | 10         | 12            | 15    | 26
Above Normal  | 5     | 7            | 9          | 11            | 14    | 25
**Normal**    | 4     | 6            | **8**      | 10            | 13    | 24
Below Normal  | 3     | 5            | 7          | 9             | 12    | 23
Lowest        | 2     | 4            | 6          | 8             | 11    | 22
Idle          | 1     | 1            | 1          | 1             | 1     | 16

- Certain priority numbers are unavailable to usermode 
  - {17-21, 27-30} are only available to kernel-mode drivers
  - 0 is only available to the zero page thread

- You would generally raise the priority of threads that need to be responsive (to user input) and reduce the priority of the thread if it is not interacting with the user

- Processes are generally assigned a priority class based on the parent process
  - Most processes are started by `explorer.exe`, so they are `Normal`.

- Within thread priorities:
  - Windows has priority level 0 and the `Real Time` range for itself
  - CLR reserves `Idle` and `Time Critical` priority levels
    - Does not use `Idle`, but the GC/Finalizer thread runs at `Time Critical`

- Again, two views of threads from .NET:
  - Windows view: `System.Diagnostics.Process` and `System.Diagnostics.ProcessThread`
  - CLR View: `AppDomain` and `Thread`


## 26.7 Foreground Threads versus Background Threads

- In the CLR, threads are either of { foreground | background }
- When all foreground threads in a process stop running, the CLR kills and background threads
  - This is done immediately, no exceptions are thrown
- Due to this: 
  - Foreground threads should be used for tasks that must complete (e.g. writing to disk)
  - Background threads should be used for non-mission critical tasks (e.g. UI updates)

```cs
using System;
using System.Threading;

public static class Program {
  public static void Main() {
    // threads default to foreground
    Thread t = new Thread(Worker);
    t.IsBackground = true;

    t.Start();

    Console.WriteLine("Returning from Main()");
  }
}

private static void Worker() {
  Thread.Sleep(10000); //simulate work by sleeping
  Console.Writeline("Returning from Worker()");
}
```

- If `t` is a background thread, "Returning from Worker()" may not print
  - As soon as Main() completes, the process will be cleaned up (including any currently-executing threads)
- A thread's { foreground | background } state may be changed at any time during its lifetime
- An application's primary thread and any explicitly created threads will default to Foreground
- Thread pool threads are by default background threads
- Any threads created by native code that enter the managed execution environment are considered background

- Avoid creating foreground threads if possible as they can prevent application termination

## 26.8 What Now?

- Threads are expensive resources!
- Use the thread pool
  - This manages thread creation/destruction for you (efficiently)