# 27.0 Compute-Bound Asynchronous Operations

[Overall TOC](../README.md)

**Contents**

[[_TOC_]]

- We're looking here at ways we can perform operations asynchronously
- This means that it will be executed on another thread
- Some examples of compute-bound operations might be:
  - Compiling code
  - Spell checking
  - Grammar checking
  - Spreadsheet recalculations
  - Transcoding audio/video
  - Producing the thumbnail of an image
- Most of the time, applications are waiting for IO to complete 
  - They're not spending all of their time processing in-memory data
  - They're more likely to be doing things like waiting for timers, data read/write operations (e.g. from a database/file/network request), waiting for keystrokes or other user input
- Even in IO bound applications, they often perform some computation on the data and so parallelizing this data can increase the application throughput
- Here, we talk about the CLR's thread pool and some concepts on how it works and how to use it
  - Thread pool allows you to design/implement responsive+scalable applications/components
  - Shows various mechanisms available that allow compute-bound operations via the thread pool

## 27.1 Introducing the CLR's Thread Pool

- Creating and destroying objects is expensive!
  - There's a .NET pattern of using "pools" - reusable resources
  - In the case of threads, the CLR manages its own threadpool: a set of threads that are available for the application's use
  - One thread pool per instance of the CLR, shared by all AppDomains controlled by that CLR instance
  - If one application loads multiple CLR instances, each will have its own thread pool
- When the CLR initializes, the thread pool is empty
  - The thread pool maintains a queue of operation requests
  - When your application wants to perform some async operation, you call a method that appends an entry to the thread pool's queue
  - The thread pool extracts entries from this queue and dispatches the queued entry to a thread pool queue
    - If there's no threads in the pool, a new thread is created
  - When the thread completes its task, it is not destroyed - instead it is just returned to the available threads in the pool
  - This means that threads are re-used, reducing the number of times that threads are created/destroyed
- If you're making a lot of requests to the thread pool, the thread pool will try to service the requests using the single thread. If the thread pool operation queue can't keep up, additional threads will be created. Eventually there will be enough threads to service your application's requests and no more threads will need to be added to the thread pool
- If there's a lot of threads sitting idle in the thread pool for some time, the thread wakes itself and commits sudoku to conserve OS resources
- Important thing is that the thread pool is managed on a heuristic - it will choose its behaviour based on the available system resources and the load on the application


## 27.2 Performing a Simple Compute-Bound Operation

## 27.3 Execution Contexts

## 27.4 Cooperative Cancellation

## 27.5 Tasks

## 27.6 `Parallel`'s Static `For`, `ForEach` and `Invoke` Methods

## 27.7 Parallel Language Integrated Query (LINQ)

## 27.8 Performing a Periodic Compute-Bound Operation

## 27.9 How the Thread Pool Manages its Threads

## 27.10 Cache Lines and False Sharing